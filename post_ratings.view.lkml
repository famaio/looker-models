view: post_ratings {
  sql_table_name: public.post_ratings ;;

  dimension: post_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.post_id ;;
  }

  dimension: rating_id {
    type: string
    sql: ${TABLE}.rating_id ;;
  }

  dimension: user_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [users.username, users.user_id, posts.post_id]
  }
}
