view: article_queries {
  sql_table_name: public.article_queries ;;

  dimension: article_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.article_id ;;
  }

  dimension: data_type {
    type: string
    sql: ${TABLE}.data_type ;;
  }

  dimension: external_api_type {
    type: string
    sql: ${TABLE}.external_api_type ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: person_data {
    type: string
    sql: ${TABLE}.person_data ;;
  }

  dimension: query_type {
    type: string
    sql: ${TABLE}.query_type ;;
  }

  dimension: quoted {
    type: yesno
    sql: ${TABLE}.quoted ;;
  }

  measure: count {
    type: count
    drill_fields: [name, articles.article_id]
  }

  measure: manual_count {
    type:  count
    filters: {
      field: data_type
      value: "MANUAL"
    }
    drill_fields: [name]
  }
}
