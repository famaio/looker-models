view: post_flags {
  sql_table_name: public.post_flags ;;

  dimension: flag {
    type: string
    sql: ${TABLE}.flag ;;
  }

  dimension: flag_type {
    type: string
    sql: ${TABLE}.flag_type ;;
  }

  dimension: post_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.post_id ;;
  }

  measure: count {
    type: count
    drill_fields: [posts.post_id]
  }
}
