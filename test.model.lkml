connection: "redshift"

# include all the views
include: "*.view"

# include all the dashboards
include: "*.dashboard"

# explore: rater_actions {

# }

# explore: companies {}

# explore: person_actions{}

# explore: reports {
#   join: users {
#     type: left_outer
#     sql_on: ${reports.user_id} = ${users.user_id} ;;
#     relationship: many_to_one
#   }

#   join: companies {
#     type: left_outer
#     sql_on: ${reports.company_id} = ${companies.company_id} ;;
#     relationship: many_to_one
#   }

#   join: articles {
#     type: left_outer
#     sql_on: ${reports.report_id} = ${articles.report_id} ;;
#     relationship: one_to_many
#   }

#   join: article_queries {
#     type: left_outer
#     sql_on: ${article_queries.article_id} = ${articles.article_id} ;;
#     relationship: one_to_many
#   }

#   join: article_flags {
#     type: left_outer
#     sql_on: ${article_flags.article_id} = ${articles.article_id} ;;
#     relationship: one_to_many
#   }

#   join: profiles {
#     type: left_outer
#     sql_on: ${reports.report_id} = ${profiles.report_id} ;;
#     relationship: one_to_many
#   }

#   join: posts {
#     type: left_outer
#     sql_on: ${reports.report_id} = ${posts.report_id} ;;
#     relationship: one_to_many
#   }

#   join: post_flags {
#     type: left_outer
#     sql_on: ${post_flags.post_id} = ${posts.post_id} ;;
#     relationship: one_to_many
#   }

#   join: rater_actions {
#     type: left_outer
#     sql_on: ${rater_actions.action_item} = ${profiles.profile_id} ;;
#     relationship: many_to_one
#     view_label: "Profile Actions"
#   }

# }

# explore: profiles {}

# explore: posts {
#   join: reports {
#     type: left_outer
#     sql_on: ${posts.report_id} = ${reports.report_id} ;;
#     relationship: many_to_one
#   }
#   join: companies {
#     type:  left_outer
#     sql_on: ${reports.company_id} = ${companies.company_id};;
#     relationship: many_to_one
#   }
# }

# explore: report_metrics {
# }

# explore: article_flags {
#   join: articles {
#     type: left_outer
#     sql_on: ${article_flags.article_id} = ${articles.article_id} ;;
#     relationship: many_to_one
#   }

#   join: reports {
#     type: left_outer
#     sql_on: ${articles.report_id} = ${reports.report_id} ;;
#     relationship: many_to_one
#   }

#   join: users {
#     type: left_outer
#     sql_on: ${reports.user_id} = ${users.user_id} ;;
#     relationship: many_to_one
#   }
# }

# explore: article_predictions {
#   join: articles {
#     type: left_outer
#     sql_on: ${article_predictions.article_id} = ${articles.article_id} ;;
#     relationship: many_to_one
#   }

#   join: article_flags {
#     type: left_outer
#     sql_on: ${article_predictions.article_id} = ${article_flags.article_id} ;;
#     relationship: one_to_many
#   }

#   join: reports {
#     type: left_outer
#     sql_on: ${articles.report_id} = ${reports.report_id} ;;
#     relationship: many_to_one
#   }

#   join: users {
#     type: left_outer
#     sql_on: ${reports.user_id} = ${users.user_id} ;;
#     relationship: many_to_one
#   }
# }

# explore: article_queries {
#   join: articles {
#     type: left_outer
#     sql_on: ${article_queries.article_id} = ${articles.article_id} ;;
#     relationship: many_to_one
#   }

#   join: article_flags {
#     type: left_outer
#     sql_on: ${article_queries.article_id} = ${article_flags.article_id} ;;
#     relationship: one_to_many
#   }

#   join: reports {
#     type: left_outer
#     sql_on: ${articles.report_id} = ${reports.report_id} ;;
#     relationship: many_to_one
#   }

#   join: users {
#     type: left_outer
#     sql_on: ${reports.user_id} = ${users.user_id} ;;
#     relationship: many_to_one
#   }
# }

# explore: article_ratings {
#   join: users {
#     type: left_outer
#     sql_on: ${article_ratings.user_id} = ${users.user_id} ;;
#     relationship: many_to_one
#   }

#   join: articles {
#     type: left_outer
#     sql_on: ${article_ratings.article_id} = ${articles.article_id} ;;
#     relationship: many_to_one
#   }

#   join: article_flags {
#     type: left_outer
#     sql_on: ${article_ratings.article_id} = ${article_flags.article_id} ;;
#     relationship: one_to_many
#   }


#   join: reports {
#     type: left_outer
#     sql_on: ${articles.report_id} = ${reports.report_id} ;;
#     relationship: many_to_one
#   }
# }

# explore: articles {
#   join: reports {
#     type: left_outer
#     sql_on: ${articles.report_id} = ${reports.report_id} ;;
#     relationship: many_to_one
#   }

#   join: article_flags {
#     type: left_outer
#     sql_on: ${articles.article_id} = ${article_flags.article_id} ;;
#     relationship: one_to_many
#   }

#   join: users {
#     type: left_outer
#     sql_on: ${reports.user_id} = ${users.user_id} ;;
#     relationship: many_to_one
#   }

#   join: article_queries {
#     type: left_outer
#     sql_on: ${article_queries.article_id} = ${articles.article_id} ;;
#     relationship: many_to_one
#   }

#   join: companies {
#     type: left_outer
#     sql_on: ${reports.company_id} = ${companies.company_id} ;;
#     relationship: many_to_one
#   }
# }
# #
# # explore: companies {}
# #
# explore: company_searches {}
# #
# # explore: notifications {}
# #
# explore: post_flags {
#   join: posts {
#     type: left_outer
#     sql_on: ${post_flags.post_id} = ${posts.post_id} ;;
#     relationship: many_to_one
#   }

#   join: post_media_entries {
#     type: left_outer
#     sql_on: ${post_flags.post_id} = ${post_media_entries.post_id} ;;
#     relationship: one_to_many
#   }

#   join: profiles {
#     type: left_outer
#     sql_on: ${posts.profile_id} = ${profiles.profile_id} ;;
#     relationship: many_to_one
#   }

#   join: reports {
#     type: left_outer
#     sql_on: ${posts.report_id} = ${reports.report_id} ;;
#     relationship: many_to_one
#   }

#   join: users {
#     type: left_outer
#     sql_on: ${reports.user_id} = ${users.user_id} ;;
#     relationship: many_to_one
#   }

#   join: companies {
#     type:  left_outer
#     sql_on: ${reports.company_id} = ${companies.company_id};;
#     relationship: many_to_one
#   }
# }
# #
# # explore: post_media_entries {
# #   join: posts {
# #     type: left_outer
# #     sql_on: ${post_media_entries.post_id} = ${posts.post_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: profiles {
# #     type: left_outer
# #     sql_on: ${posts.profile_id} = ${profiles.profile_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: reports {
# #     type: left_outer
# #     sql_on: ${posts.report_id} = ${reports.report_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: users {
# #     type: left_outer
# #     sql_on: ${reports.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# # explore: post_predictions {
# #   join: posts {
# #     type: left_outer
# #     sql_on: ${post_predictions.post_id} = ${posts.post_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: profiles {
# #     type: left_outer
# #     sql_on: ${posts.profile_id} = ${profiles.profile_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: reports {
# #     type: left_outer
# #     sql_on: ${posts.report_id} = ${reports.report_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: users {
# #     type: left_outer
# #     sql_on: ${reports.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# explore: post_ratings {
#   join: users {
#     type: left_outer
#     sql_on: ${post_ratings.user_id} = ${users.user_id} ;;
#     relationship: many_to_one
#   }

#   join: posts {
#     type: left_outer
#     sql_on: ${post_ratings.post_id} = ${posts.post_id} ;;
#     relationship: many_to_one
#   }

#   join: profiles {
#     type: left_outer
#     sql_on: ${posts.profile_id} = ${profiles.profile_id} ;;
#     relationship: many_to_one
#   }

#   join: reports {
#     type: left_outer
#     sql_on: ${posts.report_id} = ${reports.report_id} ;;
#     relationship: many_to_one
#   }

#   join: companies {
#     type: left_outer
#     sql_on: ${reports.company_id} = ${companies.company_id} ;;
#     relationship: many_to_one
#   }
# }
# #
# # explore: post_topics {
# #   join: reports {
# #     type: left_outer
# #     sql_on: ${post_topics.report_id} = ${reports.report_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: users {
# #     type: left_outer
# #     sql_on: ${reports.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# # explore: posts {
# #   join: profiles {
# #     type: left_outer
# #     sql_on: ${posts.profile_id} = ${profiles.profile_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: reports {
# #     type: left_outer
# #     sql_on: ${posts.report_id} = ${reports.report_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: users {
# #     type: left_outer
# #     sql_on: ${reports.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# # explore: profiles {
# #   join: reports {
# #     type: left_outer
# #     sql_on: ${profiles.report_id} = ${reports.report_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: users {
# #     type: left_outer
# #     sql_on: ${reports.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# # explore: rater_actions {}
# #
# # explore: rating_flags {
# #   join: users {
# #     type: left_outer
# #     sql_on: ${rating_flags.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# # explore: report_addresses {
# #   join: reports {
# #     type: left_outer
# #     sql_on: ${report_addresses.report_id} = ${reports.report_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: users {
# #     type: left_outer
# #     sql_on: ${reports.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# # explore: report_aliases {
# #   join: reports {
# #     type: left_outer
# #     sql_on: ${report_aliases.report_id} = ${reports.report_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: users {
# #     type: left_outer
# #     sql_on: ${reports.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# # explore: report_degrees {
# #   join: reports {
# #     type: left_outer
# #     sql_on: ${report_degrees.report_id} = ${reports.report_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: users {
# #     type: left_outer
# #     sql_on: ${reports.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# # explore: report_emails {
# #   join: reports {
# #     type: left_outer
# #     sql_on: ${report_emails.report_id} = ${reports.report_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: users {
# #     type: left_outer
# #     sql_on: ${reports.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# # explore: report_jobs {
# #   join: reports {
# #     type: left_outer
# #     sql_on: ${report_jobs.report_id} = ${reports.report_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: users {
# #     type: left_outer
# #     sql_on: ${reports.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# # explore: report_phones {
# #   join: reports {
# #     type: left_outer
# #     sql_on: ${report_phones.report_id} = ${reports.report_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: users {
# #     type: left_outer
# #     sql_on: ${reports.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# # explore: reports {
# #   join: users {
# #     type: left_outer
# #     sql_on: ${reports.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# # explore: schema_migrations {}
# #
# # explore: user_clearances {
# #   join: users {
# #     type: left_outer
# #     sql_on: ${user_clearances.user_id} = ${users.user_id} ;;
# #     relationship: many_to_one
# #   }
# # }
# #
# # explore: users {}
