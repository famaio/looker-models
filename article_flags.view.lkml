view: article_flags {
  sql_table_name: public.article_flags ;;

  dimension: article_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.article_id ;;
  }

  dimension: flag {
    type: string
    sql: ${TABLE}.flag ;;
  }

  dimension: flag_type {
    type: string
    sql: ${TABLE}.flag_type ;;
  }

  measure: count {
    type: count
    drill_fields: [articles.article_id]
  }
}
