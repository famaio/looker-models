view: article_ratings {
  sql_table_name: public.article_ratings ;;

  dimension: article_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.article_id ;;
  }

  dimension: rating_id {
    type: string
    sql: ${TABLE}.rating_id ;;
  }

  dimension: user_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [users.username, users.user_id, articles.article_id]
  }
}
