view: article_predictions {
  sql_table_name: public.article_predictions ;;

  dimension: article_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.article_id ;;
  }

  dimension: model_type {
    type: string
    sql: ${TABLE}.model_type ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: prediction_type {
    type: string
    sql: ${TABLE}.prediction_type ;;
  }

  dimension: value {
    type: number
    sql: ${TABLE}.value ;;
  }

  measure: count {
    type: count
    drill_fields: [name, articles.article_id]
  }
}
