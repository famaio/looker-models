include: "test.model.lkml"

view: report_metrics {
  derived_table: {
    sql_trigger_value:  SELECT FLOOR((UNIX_TIMESTAMP(NOW()) - 60*60*3)/(60*60*24));;
    distribution_style: all
    explore_source: reports {
      column: article_count { field: articles.count }
      column: article_relevant_or_flagged_count {field: articles.relevant_count}
      column: company_name { field: companies.proper_case_name }
      column: company_id { field: company_id }
      column: date_created_date {}
      column: date_finished_date {}
      column: deleted {}
      column: paused {}
      column: person_id {}
      column: profile_big_four_count { field: profiles.big_four_count}
      column: profile_count { field: profiles.count }
      column: profile_confirmed_count { field: profiles.confirmed_count }
      column: post_count { field: posts.count }
      column: report_id {}
      column: report_run_duration {}
      column: search_id {}
      column: status {}
      column: screening_user { field: users.username }
      column: visible_priority {}
    }
  }
  dimension: article_count {
    type: number
  }
  dimension: article_relevant_or_flagged_count {
    type: number
  }
  dimension: company_name {}
  dimension: company_id {
    hidden:  yes
  }
  dimension: date_created_date {}
  dimension: date_finished_date {}
  dimension: deleted {}
  dimension: paused {}
  dimension: person_id {}
  dimension: profile_count {
    type: number
  }
  dimension: profile_big_four_count {
    type: number
  }
  dimension: profile_confirmed_count {
    type: number
  }
  dimension: post_count {
    type: number
  }
  dimension: report_run_duration {}
  dimension: report_id {}
  dimension: search_id {}
  dimension: status {}
  dimension: screening_user {}
  dimension: visible_priority {}

  measure: count {
    type: count_distinct
    sql:  ${person_id} ;;
    drill_fields: [detail*]
  }

  measure: sum_of_article_count {
    type:  sum
    sql: ${article_count} ;;
  }

  measure: sum_of_article_relevant_or_flagged_count {
    type:  sum
    sql: ${article_relevant_or_flagged_count} ;;
  }

  measure: sum_of_profile_count {
    type:  sum
    sql: ${profile_count} ;;
  }

  measure: sum_of_profile_big_four_count {
    type:  sum
    sql: ${profile_big_four_count} ;;
  }

  measure: sum_of_profile_confirmed_count {
    type:  sum
    sql: ${profile_confirmed_count} ;;
  }

  measure: sum_of_post_count {
    type:  sum
    sql: ${post_count} ;;
  }

  set: detail {
    fields: [
      company_name,
      date_created_date,
      date_finished_date,
      deleted,
      paused,
      person_id,
      search_id,
      status,
      screening_user,
      visible_priority
    ]
  }
}
