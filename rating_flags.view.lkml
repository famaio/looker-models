view: rating_flags {
  sql_table_name: public.rating_flags ;;

  dimension: flag {
    type: string
    sql: ${TABLE}.flag ;;
  }

  dimension: flag_type {
    type: string
    sql: ${TABLE}.flag_type ;;
  }

  dimension: rating_id {
    type: string
    sql: ${TABLE}.rating_id ;;
  }

  dimension: user_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [users.username, users.user_id]
  }
}
