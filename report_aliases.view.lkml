view: report_aliases {
  sql_table_name: public.report_aliases ;;

  dimension: alias {
    type: string
    sql: ${TABLE}.alias ;;
  }

  dimension: report_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.report_id ;;
  }

  measure: count {
    type: count
    drill_fields: [reports.first_name, reports.middle_name, reports.last_name, reports.report_id]
  }
}
