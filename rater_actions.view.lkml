view: rater_actions {
  sql_table_name: public.rater_actions ;;

  dimension: accuracy {
    type: number
    sql: ${TABLE}.accuracy ;;
  }

  dimension: action_item {
    type: string
    sql: ${TABLE}.action_item ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.end_time ;;
  }

  dimension: rater_id {
    type: string
    sql: ${TABLE}.rater_id ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day
    ]
    sql: ${TABLE}.start_time ;;
  }

  dimension: duration {
    type: number
    sql: CASE WHEN ${end_date} IS NOT NULL THEN datediff('seconds', ${start_raw}, ${end_raw}) END;;
  }

  dimension: duration_minutes {
    type: number
    sql: CASE WHEN ${end_date} IS NOT NULL THEN datediff('minutes', ${start_raw}, ${end_raw}) END;;
  }

  measure: duration_average {
    type: average
    sql: ${duration} ;;
  }

  measure: duration_average_minutes {
    type: average
    sql: ${duration_minutes} ;;
  }

  measure: duration_median_minutes {
    type: median
    sql: ${duration_minutes} ;;
  }

  measure: duration_sum {
    type: sum
    sql: ${duration} ;;
  }

  measure: duration_sum_minutes {
    type: sum
    sql: CASE WHEN ${duration_minutes} <= 180 THEN ${duration_minutes} END;;
  }

   dimension: action_type {
    type: string
    sql: ${TABLE}.action_type ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }

  measure: count {
    type: count
    drill_fields: [username]
  }
}
