# Looker Models #

### What is this repository for? ###

This repository is used for storing and updating looker models. Looker recommends we do this. 
Saving the LookML models to git allows us to view previous versions of models as well as have 
a centralized storage space that will last through any server issues we may have. 
Models are saved to git via the Looker interface.