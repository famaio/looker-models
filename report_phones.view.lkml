view: report_phones {
  sql_table_name: public.report_phones ;;

  dimension: phone_number {
    type: string
    sql: ${TABLE}.phone_number ;;
  }

  dimension: report_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.report_id ;;
  }

  measure: count {
    type: count
    drill_fields: [reports.first_name, reports.middle_name, reports.last_name, reports.report_id]
  }
}
