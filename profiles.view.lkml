view: profiles {
  sql_table_name: public.profiles ;;

  dimension: profile_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.profile_id ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: display_name {
    type: string
    sql: ${TABLE}.display_name ;;
  }

  dimension: handle {
    type: string
    sql: ${TABLE}.handle ;;
  }

  dimension: picture_url {
    type: string
    sql: ${TABLE}.picture_url ;;
  }

  dimension: report_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.report_id ;;
  }

  dimension: search_id {
    type: string
    sql: ${TABLE}.search_id ;;
  }

  dimension: site_id {
    type: string
    sql: ${TABLE}.site_id ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension: url {
    type: string
    sql: ${TABLE}.url ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: confirmed_count {
    type:  count
    filters: {
      field: status
      value: "CONFIRMED"
    }
    drill_fields: [detail*]
  }

  measure: big_four_count {
    type:  count_distinct
    sql: CASE WHEN ${status} = 'CONFIRMED' AND (${type} = 'FACEBOOK' OR ${type} = 'GOOGLEPLUS' OR ${type} = 'INSTAGRAM' OR ${type} = 'TWITTER')
      THEN ${profile_id}
      ELSE NULL
      END;;
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      profile_id,
      display_name,
      reports.first_name,
      reports.middle_name,
      reports.last_name,
      reports.report_id,
      posts.count
    ]
  }
}
