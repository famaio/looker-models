view: reports {
  sql_table_name: public.reports ;;

  dimension: report_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.report_id ;;
  }

  dimension: report_link {
    type: string
    sql: 'https://web.fama.io/#/person/' || ${TABLE}.report_id || '/report' ;;
  }

  dimension: company_id {
    type: string
    sql: ${TABLE}.company_id ;;
  }

  dimension_group: date_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      hour_of_day,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_created ;;
  }

  measure : most_recent_report_run {
    type: date
    sql: MAX(${date_created_date}) ;;
    convert_tz: no
  }

#   dimension_group: adjusted_date_created {
#     type: time
#     timeframes: [
#       time
#     ]
#     sql: CASE WHEN ${date_created_time} >=  AND ${date_created_time} <=  THEN dateadd('hours', ${date_created_time}, 48) ;;
#   }

  dimension_group: date_finished {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_finished ;;
  }

  dimension: report_run_duration {
    type: number
    sql: CASE WHEN ${date_finished_date} != '1970-01-01' THEN datediff('minutes', ${date_created_raw}, ${date_finished_raw})/60 END;;
  }

  measure: average_report_duration {
    type: average
    drill_fields: [report_details*]
    sql: ${report_run_duration} ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: frca_disclosure_password {
    type: string
    sql: ${TABLE}.frca_disclosure_password ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.last_name ;;
  }

  dimension: middle_name {
    type: string
    sql: ${TABLE}.middle_name ;;
  }

  dimension: paused {
    type: yesno
    sql: ${TABLE}.paused ;;
  }

  dimension: person_id {
    type: string
    sql: ${TABLE}.person_id ;;
  }

  dimension: role {
    type: string
    sql: ${TABLE}.role ;;
  }

  dimension: search_id {
    type: string
    sql: ${TABLE}.search_id ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: user_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  dimension: visible_priority {
    type: string
    sql: ${TABLE}.visible_priority ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      report_id,
      first_name,
      middle_name,
      last_name,
      users.username,
      users.user_id,
      articles.count,
      post_topics.count,
      posts.count,
      profiles.count,
      report_addresses.count,
      report_aliases.count,
      report_degrees.count,
      report_emails.count,
      report_jobs.count,
      report_phones.count
    ]
  }

  set: report_details {
    fields: [
      date_created_time,
      date_finished_time,
      report_run_duration,
      company_id,
      first_name,
      last_name
    ]
  }
}
