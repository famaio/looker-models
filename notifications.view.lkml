view: notifications {
  sql_table_name: public.notifications ;;

  dimension: notification_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.notification_id ;;
  }

  dimension_group: date_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_created ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: email {
    type: yesno
    sql: ${TABLE}.email ;;
  }

  dimension: message {
    type: string
    sql: ${TABLE}.message ;;
  }

  dimension: slack {
    type: yesno
    sql: ${TABLE}.slack ;;
  }

  measure: count {
    type: count
    drill_fields: [notification_id]
  }
}
