view: company_searches {
  sql_table_name: public.company_searches ;;

  dimension: company_search_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.company_search_id ;;
  }

  dimension: callback_url {
    type: string
    sql: ${TABLE}.callback_url ;;
  }

  dimension: callbacks {
    type: string
    sql: ${TABLE}.callbacks ;;
  }

  dimension: company_id {
    type: string
    sql: ${TABLE}.company_id ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: fcra_contract_group {
    type: string
    sql: ${TABLE}.fcra_contract_group ;;
  }

  dimension: keywords {
    type: string
    sql: ${TABLE}.keywords ;;
  }

  dimension: lookback_window {
    type: number
    sql: ${TABLE}.lookback_window ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: search_id {
    type: string
    sql: ${TABLE}.search_id ;;
  }

  dimension: sensitivities {
    type: string
    sql: ${TABLE}.sensitivities ;;
  }

  dimension: stemmable_keywords {
    type: string
    sql: ${TABLE}.stemmable_keywords ;;
  }

  measure: count {
    type: count
    drill_fields: [company_search_id, name]
  }
}
