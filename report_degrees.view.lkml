view: report_degrees {
  sql_table_name: public.report_degrees ;;

  dimension: educator {
    type: string
    sql: ${TABLE}.educator ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.end_date ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: report_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.report_id ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start_date ;;
  }

  measure: count {
    type: count
    drill_fields: [name, reports.first_name, reports.middle_name, reports.last_name, reports.report_id]
  }
}
