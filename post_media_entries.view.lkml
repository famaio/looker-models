view: post_media_entries {
  sql_table_name: public.post_media_entries ;;

  dimension: media_type {
    type: string
    sql: ${TABLE}.media_type ;;
  }

  dimension: post_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.post_id ;;
  }

  dimension: thumbnail_url {
    type: string
    sql: ${TABLE}.thumbnail_url ;;
  }

  dimension: url {
    type: string
    sql: ${TABLE}.url ;;
  }

  measure: count {
    type: count
    drill_fields: [posts.post_id]
  }
}
