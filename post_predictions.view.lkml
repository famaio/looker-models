view: post_predictions {
  sql_table_name: public.post_predictions ;;

  dimension: model_type {
    type: string
    sql: ${TABLE}.model_type ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: post_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.post_id ;;
  }

  dimension: prediction_type {
    type: string
    sql: ${TABLE}.prediction_type ;;
  }

  dimension: value {
    type: number
    sql: ${TABLE}.value ;;
  }

  measure: count {
    type: count
    drill_fields: [name, posts.post_id]
  }
}
