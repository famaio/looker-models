view: posts {
  sql_table_name: public.posts ;;

  dimension: post_id {
    primary_key: yes
    link: {
      label: "Original Post"
      url: "{{url._value}}"
      icon_url: "http://google.com/favicon.ico"
    }
    type: string
    sql: ${TABLE}.post_id ;;
  }

  dimension_group: date_posted {
    type: time
    timeframes: [
      raw,
      time,
      time_of_day,
      hour_of_day,
      day_of_week,
      date,
      week,
      month,
      month_name,
      quarter,
      year
    ]
    sql: ${TABLE}.date_posted ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: hidden {
    type: yesno
    sql: ${TABLE}.hidden ;;
  }

  dimension: post_site_id {
    type: string
    sql: ${TABLE}.post_site_id ;;
  }

  dimension: post_type {
    type: string
    sql: ${TABLE}.post_type ;;
  }

  dimension: probability {
    type: number
    sql: ${TABLE}.probability ;;
  }

  dimension: profile_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.profile_id ;;
  }

  dimension: report_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.report_id ;;
  }

  dimension: search_id {
    type: string
    sql: ${TABLE}.search_id ;;
  }

  dimension: text {
    type: string
    sql: ${TABLE}.text ;;
  }

  dimension: url {
    type: string
    sql: ${TABLE}.url ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      post_id,
      profiles.display_name,
      profiles.profile_id,
      reports.first_name,
      reports.middle_name,
      reports.last_name,
      reports.report_id,
      post_flags.count,
      post_media_entries.count,
      post_predictions.count,
      post_ratings.count
    ]
  }
}
