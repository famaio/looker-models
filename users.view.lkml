view: users {
  sql_table_name: public.users ;;

  dimension: user_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.user_id ;;
  }

  dimension: available_actions_csv {
    type: string
    sql: ${TABLE}.available_actions_csv ;;
  }

  dimension: company_id {
    type: string
    sql: ${TABLE}.company_id ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: password_reset {
    type: yesno
    sql: ${TABLE}.password_reset ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      user_id,
      username,
      article_ratings.count,
      post_ratings.count,
      rating_flags.count,
      reports.count,
      user_clearances.count
    ]
  }
}
