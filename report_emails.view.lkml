view: report_emails {
  sql_table_name: public.report_emails ;;

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: report_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.report_id ;;
  }

  measure: count {
    type: count
    drill_fields: [reports.first_name, reports.middle_name, reports.last_name, reports.report_id]
  }
}
