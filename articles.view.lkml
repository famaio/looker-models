view: articles {
  sql_table_name: public.articles ;;

  dimension: article_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.article_id ;;
  }

  dimension_group: date_posted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_posted ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: hidden {
    type: yesno
    sql: ${TABLE}.hidden ;;
  }

  dimension: probability {
    type: number
    sql: ${TABLE}.probability ;;
  }

  dimension: relevant {
    type: yesno
    sql: ${TABLE}.relevant ;;
  }

  dimension: report_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.report_id ;;
  }

  dimension: search_id {
    type: string
    sql: ${TABLE}.search_id ;;
  }

  dimension: text {
    type: string
    sql: ${TABLE}.text ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  dimension: probablity_flag {
    type: yesno
    sql: ${probability} > .5 ;;
  }

  dimension: url {
    type: string
    sql: ${TABLE}.url ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: relevant_count {
    type:  count
    filters: {
      field: relevant
      value: "yes"
    }
    drill_fields: [detail*]
  }

  measure: probablity_threshold_count {
    type:  count
    filters: {
      field: probablity_flag
      value: "yes"
    }
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      article_id,
      reports.first_name,
      reports.middle_name,
      reports.last_name,
      reports.report_id,
      article_flags.count,
      article_predictions.count,
      article_queries.count,
      article_ratings.count
    ]
  }
}
