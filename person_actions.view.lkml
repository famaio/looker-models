include: "test.model.lkml"

view: person_actions{
  derived_table: {
    explore_source: rater_actions {
      column: person_id {field: rater_actions.action_item}
      column: action_type {}
      column: start_time {field: rater_actions.start_time}
      column: end_time {field: rater_actions.end_time}
      column: duration {}
      column: duration_minutes {}
      column: rater_id {}
      column: username {}
      filters: {
        field: rater_actions.action_type
        value: "MANUAL^_PERSON^_CONFIRM,MANUAL^_PERSON^_QA"
      }
    }
  }
  dimension: person_id {}
  dimension: action_type {}
  dimension: duration {}
  dimension: duration_minutes {}
  dimension: rater_id {}
  dimension: username {}

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: rater_action.start_time ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: rater_action.end_time ;;
  }
}
