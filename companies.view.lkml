view: companies {
  sql_table_name: public.companies ;;

  dimension: company_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.company_id ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: lower_case_name {
    type: string
    sql: ${TABLE}.lower_case_name ;;
  }

  dimension: msa_contract_group {
    type: string
    sql: ${TABLE}.msa_contract_group ;;
  }

  dimension: news_probability {
    type: yesno
    sql: ${TABLE}.news_probability ;;
  }

  dimension: proper_case_name {
    type: string
    sql: ${TABLE}.proper_case_name ;;
  }

  dimension: segregate_users {
    type: yesno
    sql: ${TABLE}.segregate_users ;;
  }

  measure: count {
    type: count
    drill_fields: [company_id, proper_case_name, lower_case_name]
  }
}
