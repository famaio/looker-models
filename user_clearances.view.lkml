view: user_clearances {
  sql_table_name: public.user_clearances ;;

  dimension: admin_clearance {
    type: yesno
    sql: ${TABLE}.admin_clearance ;;
  }

  dimension: awaiting_signature_clearance {
    type: yesno
    sql: ${TABLE}.awaiting_signature_clearance ;;
  }

  dimension: rater_clearance {
    type: yesno
    sql: ${TABLE}.rater_clearance ;;
  }

  dimension: superadmin_clearance {
    type: yesno
    sql: ${TABLE}.superadmin_clearance ;;
  }

  dimension: user_clearance {
    type: yesno
    sql: ${TABLE}.user_clearance ;;
  }

  dimension: user_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  dimension: viewer_clearance {
    type: yesno
    sql: ${TABLE}.viewer_clearance ;;
  }

  measure: count {
    type: count
    drill_fields: [users.username, users.user_id]
  }
}
